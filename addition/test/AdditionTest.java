/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import fi.helsinki.cs.tmc.edutestutils.Points;

/**
 *
 * @author grettte
 */
@Points("5")
public class AdditionTest {
    int res1 = 0;
    int res2 = 10;
    int res;
    public AdditionTest() {
    }
    
    @Test
    public void test() {
        res = Addition.additionne(8, -8);
        assertTrue("La fonction n'additionne pas",res == res1);
        res = Addition.additionne(7, 3);
        assertTrue("La fonction n'additionne pas",res == res2);
    }
}
