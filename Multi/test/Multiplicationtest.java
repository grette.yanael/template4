/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import fi.helsinki.cs.tmc.edutestutils.Points;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yanaël
 */

@Points("5")
public class Multiplicationtest {
    double res;
    double res1= 100;
    double res2 = 0;
    @Test
    public void test() {
        res = Multiplication.Multiplication(10, 10);
        assertTrue("test de multiplication échoué",res == res1);
        res = Multiplication.Multiplication(646855, 0);
        assertTrue("test de multiplication échoué",res == res2);
    }
}
